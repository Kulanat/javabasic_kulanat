package day1;

import java.util.ArrayList;

public class Lab6_kulanat {
	public static void main(String[] args) {
		
//		for(int counter = 5; counter <= 10; counter =counter+2) {
//		      System.out.println("Counter :" + counter);
//		}
//		int [] pageNumber = {1,2,3,4,5,6};	
//		int [] walletBath = {20,40,100};	
//		int [] testScore = {20,20,5};
//		String [] friends = {"Touch","Via","Ice"};
//		
//		//		for (int i = 0 ; i < pageNumber.length; i++) {
////			System.out.println(pageNumber[i]);
////		}			
////		for (int i = 0 ; i < walletBath.length; i++) {
////			System.out.println(walletBath[i]);
////		}
//		for(String member : friends) {
//			System.out.println(member);
//		}
//		int counter = 0;
//		while (counter < 5) {
//			System.out.println("Counter" + counter);
//			counter++;			
//		}
		// 1
		int counter = 1;
		while (counter <10) {
			System.out.println(counter);
			counter++;
		}
		// 2 โปรแกรมหาผลรวม ของตัวเลข 1-1 
		// 1 2 3 4 5 6 7 8 9 10
		// ต้อง + ทีละคู่ แล้วจำค่าไว้ เพื่อบวกต่อ
		// แสดงว่าต้องมีตัวชั่วคราว ที่เป็นผลรวม
		int sum = 0;
		int counter2 = 1;
		while (counter2 <=10) {
			sum = sum + counter2;			
			System.out.println("Counter "+ counter2);
			System.out.println("Sum "+ sum);
			counter2++;			
			
		}
		System.out.println("Total "+ sum);
		// 3 สร้างโปรแกรมหาค่าระหว่าง 1-100 ที่หาร 12 ลงตัว
		// เอา 12 ไปหารแล้ว เหลือเศษ = 0
		// % หารเอาเศษ ดังนั้นมันคือค่าที่ % 12 = 0
		// 1) วน Loop ตั้งแต่ 1 จนถึง 100
		// 2) ถ้าค่าตอนนั้น % 12 = 0 ให้ Print ออกมา 
		
		ArrayList<Integer> resultList = new ArrayList<Integer>();		
		int counter3 = 1;
		while(counter3 <= 100) {
			if(counter3 % 12 == 0) {
				resultList.add(counter3);				
				System.out.println("1) ตัวเลขนี้หารด้วย 12 ลงตัว " + counter3);
			}
			counter3 ++;
		}
		for (Integer res : resultList) {
			System.out.println("2) ตัวเลขนี้หารด้วย 12 ลงตัว " + res);
		}		
	}
}
