package day1;

public class Lab9_kulanat {
	 /*กำหนดตัวแปร count สำหรับนับจำนวนรอบ 
    1. count เริ่มต้นที่ 0 และไปจบที่ 20 (รวม 20 ด้วย)
    2. ถ้า count มีค่าเป็น 11 คำสั่ง continue ภายใน if จะทำงาน 
    3. ให้แสดงค่า 11 และเริ่มต้นรอบใหม่โดยไม่สนใจคำสั่งที่เหลือด้านล่าง
    */
public static void main(String[] args) {
 int i;
 
   for (i = 0; i <= 20; i++) {
     if (i ==11)
     {    
       continue;
     }
    System.out.println("รอบที่ :" + i);
   }
 System.out.println("END");
   }
 }


