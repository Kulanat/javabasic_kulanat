package day1;

public class Lab12_kulanat {
	/* กำหนด String เป็นดังนี้
    String1 = ‘You and Me’ , String2 = ‘ you and me ‘
    1. ทดลองเปรียบเทียบ String 2 String ว่าเป็นค่าเดียวกันหรือไม่
    2. ใช้คำสั่งค้นหาคำใน String และแสดงคำที่ค้นหาบนหน้าจอ
    3. ใช้คำสั่งหาความยาวของ String นั้น และแสดงค่าความยาว String 
    4. ใช้คำสั่งตัดข้อความหรือตัด String ตำแหน่งที่ 1-4 ออก
    5. ใช้คำสั่งตัดช่องว่างของประโยค
    6. ใช้คำสั่งเปลี่ยน String เป็นพิมพ์ใหญ่ทั้งหมด
    7. ใช้คำสั่งเปลี่ยน String2 เป็นพิมพ์ใหญ่ทั้งหมด และ ไม่มีช่องว่างซ้ายขวา ด้วยการเขียน code แค่บรรทัดเดียว (ใช้ Chaining นั่นเอง)
    */

public static void main(String[] args) {

String word1 = "You and Me";
String word2 = " you and me ";

//3. length        
System.out.println("Word 1 length : " + word1.length());
System.out.println("Word 2 length : " + word2.length());

//4. substring
String word = "good morning";
System.out.println(word.substring(4));

//System.out.println(word.substring(0, 4));
//System.out.println(word.substring(0, 20));



//5. trim blank space
String mySpaceWord = "   my space bar   ";
//System.out.println(mySpaceWord + " length: " + mySpaceWord.length());
String trimWord = mySpaceWord.trim();
System.out.println(trimWord);

//6. upper case
String myName = "kulanat haranda";
System.out.println(myName.toUpperCase());


//1. equal()
if ( word1.equals(word2) )
   System.out.println( "Equal" );
else
   System.out.println("Not equal");

//7. method chaining

System.out.println(setCapital(word2));
}
static String setCapital(String word2) {
return word2.substring(0,1).toUpperCase()
+ word2.substring(1);
}
}

//2.
//public   String firstname;
//public   String lastname;
//private  int age;
//
//public Employee(String firstnameInput, String lastnameInput, int ageInput) {
//   firstname = firstnameInput;
//   lastname = lastnameInput;
//   age = ageInput;
//   int temp = 123;
//}
//public void hello() {
//   System.out.println("Hello " + firstname );
//}
//public int getAge() {
//   return age;
//}
//System.out.println(setCapital(word1));
//}
//static String setCapital(String name) {
//String firstChar = name.substring(0,1);
//String upperCasefirstChar = firstChar.toUpperCase();
//String restOfName = name.substring(1);
//return upperCasefirstChar + restOfName;
//}
//}


